# README

## About

Hi! 👋 I'm Nick Nguyen (pronounced like "win"), the Senior Engineering Manager for the [Enablement: Data Stores]() stage at GitLab. I started at GitLab in September 2019 as Engineering Manager, first for the Ecosystem team, then the Geo team. Before GitLab, I worked for a fantasy sports analytics company and as a freelance developer. In a past life, I was also CEO of a chain of tea bars with 6 locations and a wholesale business. 

## Work Style and Management Philosophy

* I'm a "Yes and" communicator. This is a concept taken from the improv world that applies especially well to business communication. Tina Fey's book [Bossy Pants](https://www.goodreads.com/book/show/9418327-bossypants) has a great introduction to this and is generally a fun read. 
* I view myself as someone who "supports" my teams versus "manages" them and try to incorporate many of the concepts of [servant leadership](https://en.wikipedia.org/wiki/Servant_leadership) into my daily work.
* I usually keep work hours between 8am-5pm US central (1300-2200 UTC). However I often like to run errands, take naps, or enjoy outdoor time in the afternoons and will then do some work at night. I also find it is sometimes easier to focus later in the evening. 
* Skip-level chats: I encourage all "skip-level" reports to schedule chats with me regularly. Skip-level chats are a good way for me to increase my understanding of what is happening within teams and to provide team members with an open line of communication to provide feedback or learn more about what’s happening across the organization. I think it’s important to build a relationship to feel comfortable discussing work topics, so these don’t just need to be work focused, but can also just generally be social chats. 
    * I’ve blocked off alternating slots on Thursdays for skip-level chats (1300-1430 UTC and 1930-2100 UTC). Feel free to schedule a 15 or 30 min block. These can be one-off or recurring. If you want a recurring chat, I recommend every 4 or 6 weeks. We can also schedule outside of these blocks if there’s a time that works better for you.

## Engineering Management Resources and Recommendations

Here are some books and resources I recommend to engineers who are considering the management track:

* The Manager's Path by Camille Fournier - https://www.oreilly.com/library/view/the-managers-path/9781491973882/
* An Elegant Puzzle: Systems of Engineering Management by Will Larson - https://lethain.com/elegant-puzzle/

## Outside of Work

* My wife, Dayna, and I live on a [small farm](https://www.nettlevalleyfarm.com/). She raises pastured pigs, goats, and chickens. I help with chores and farm projects when I can, and it's a nice break from the screen.
* I enjoy a variety of music, but I'm especially passionate about classical music. I played piano growing up and picked up the violin as an adult. We have a local community orchestra that I've played in for a few years now. 
* I'm a big fan of board games and card games. A few favorites - Chess (did you know GitLab has a #chess Slack channel and club on chess.com?), Wingspan, Sushi Go, Codenames, Settlers of Catan, Terra Mystica, and tabletop wargames like Necromunda.


## Personality Tests

* [16 types (Myers-Briggs)](https://www.16personalities.com/personality-types) - INTJ or ISTJ
* [Enneagram](https://en.wikipedia.org/wiki/Enneagram_of_Personality) - 3
* [Working Genius](https://www.workinggenius.com/) - E, W, T, I, G, D
